
#include <ESP8266WiFi.h>      // Wi-Fi library for ESP8266
//#include <WiFi.h>             // Wi-Fi library for ESP32

//#include <WiFiClient.h>

#include <PubSubClient.h>     // MQTT messaging library

//const char* ssid = "UNATCO 8";
//const char* password = "RNWNK86XXF";

const char* ssid = "DesignTactix";
const char* password = "Bust@r2000";

//const char* ssid = "SPARK-UHQHSV";
//const char* password = "CXJGHNVC9K";

//const char* ssid = "VP S8 hostpot";
//const char* password = "zxru2658";

//const char* ssid = "TL-WA801ND";
//const char* password = "dinger5353";


#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  4        /* Time ESP32 will go to sleep (in seconds) */

#define HOUR_SLEEP 3600000000 /* Microseconds in 1 hour*/

#define RELAY D6 //Pin for Relay GPIO - D6 (12) for WEMOS D1


// MQTT messaging system config (connection to MQTT Broker server)
const char* mqttServer = "node02.myqtthub.com";
const int   mqttPort = 1883;
const char* mqttUser = "prembel";
const char* mqttPassword = "nanFRm1o-SXm3q1gd";

int PARAM_wakedelay = 60; //secunds, 60 seconds is 1 minute
int PARAM_numwakesbeforerelayon = 0; //0 is never turn relay on unless message says so
int PARAM_relayontime = 60; //seconds to keep relay on

int PARAM_forcerelayon = 0; //1 = force relay on now. 0 = no force, rely on auto function

WiFiClient espClient;

PubSubClient mqttClient(espClient);     // MQTT client for publish/subscribe

//RTC_DATA_ATTR int bootCount = 0;


void setup(){
  
  Serial.begin(115200);
  delay(500);
  Serial.println("Starting");
  //Increment boot number and print it every reboot
  //++bootCount;
  //Serial.println("Boot number: " + String(bootCount));

//  WiFi.mode(WIFI_STA);
//  delay(500);
  WiFi.begin(ssid, password);             // Connect to the network
  delay(500);
  mqttClient.setServer(mqttServer, mqttPort);
  mqttClient.setCallback(mqttRecieved);
  delay(500);

  Serial.println("about to connect");
  //esp_sleep_enable_timer_wakeup(4 * uS_TO_S_FACTOR);
  delay(500);

  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, LOW);
  delay(500);

}


void loop(){

  if (!mqttClient.connected())
    mqttConnect();

  mqttClient.loop();

  //Update sensor readings to MQTT broker every 1 minute
//  const unsigned long sampleDelay = 1 * 60 * 1000UL; // 1 minute
//  static unsigned long lastSampleTime = 0 - sampleDelay;  // initialize such that a reading is due the first time through loop()
//  unsigned long now = millis();

  if(PARAM_forcerelayon == 1)
  {
    digitalWrite(RELAY, HIGH); //FORCE ON Rpi
  } if(PARAM_forcerelayon == 0)
  {
    digitalWrite(RELAY, LOW); //FORCE ON Rpi
  }
//  else if (now - lastSampleTime >= sampleDelay )  {
    
//    lastSampleTime += sampleDelay;

//    Serial.println("Going to sleep now");
//    digitalWrite(RELAY, LOW); //Turn off Rpi
//    delay(1000);
//    Serial.flush(); 
//    esp_deep_sleep_start();
//  }

  delay(1000);

}


void mqttRecieved(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  String inString((char*)payload);

  if (strcmp(topic,"iobee/forcerelayon")==0) {
    if (payload[0] == '1')
      PARAM_forcerelayon = 1;
    else
      PARAM_forcerelayon = 0;
  } else if (strcmp(topic,"iobee/wakedelay")==0) {
      PARAM_wakedelay = inString.toInt();
  } else if (strcmp(topic,"iobee/numwakesbeforerelayon")==0) {
      PARAM_numwakesbeforerelayon = inString.toInt();
  } else if (strcmp(topic,"iobee/relayontime")==0) {
      PARAM_relayontime = inString.toInt();
  }



 
  Serial.println();
  Serial.println("-----------------------");
 
}


void wifiConnect(){
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");
  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }
  Serial.println('\n');
  Serial.println("Wifi connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
}


void mqttConnect() {

  delay(250);
  if(WiFi.status() != WL_CONNECTED)
    wifiConnect();
  delay(250);

  while (!mqttClient.connected()) {
    Serial.println("\nConnecting to MQTT...");
    if (mqttClient.connect("IOBeeTest", mqttUser, mqttPassword )) {
      Serial.println("connected");
    } else {
      Serial.print("failed with state ");
      Serial.print(mqttClient.state() + "\n");
      delay(5000);//Retry every 5 seconds
      if(WiFi.status() != WL_CONNECTED)
        wifiConnect();
      delay(250);
    }
  }

  mqttClient.subscribe("iobee/wakedelay");
  mqttClient.subscribe("iobee/numwakesbeforerelayon");
  mqttClient.subscribe("iobee/relayontime");
  mqttClient.subscribe("iobee/forcerelayon");

}
