How to set up build environment

1. Install latest Arduino IDE

2. Install Wemos Serial Driver for Windows
https://wiki.wemos.cc/downloads

3. Install MQTT library (PubSubClient)
- In Arduino IDE, press Ctrl+Shift+I to open Libary Manager
- Search for PubSubClient and install latest version of the library

4. Install ESP8266 board support for Arduino
- Go to: https://github.com/esp8266/Arduino
- Follow instructions under "Installing with Boards Manager"

5. Plug in ESP8266 device into USB port of PC, and select correct settings
- In Arduino IDE, go to Tools > Boards menu and select "LOLIN(WEMOS) D1 R2 & Mini"
- Find out which COM Port your Arduino is connected to (try looking in Windows Device Manager)
- Select the correct COM Port from Tools > Port menu.

6. Test compiling and uploading the code to arduino.

7. For debug console messages, open the Serial Monitor from Tools menu or Ctrl+Shift+M.
- Make sure you select the right Baud rate in the Serial Monitor.
- There will be a line of code like "Serial.begin(115200);" in the sketch which will tell you the correct Baud rate.

8. For projects using Wifi, you might need to configure the SSID and password in the sketch code.
- The variables for SSID and password are usually near the top.
